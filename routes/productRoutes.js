const express = require("express");
const router = express.Router();
const productController = require("../controllers/productControllers");
const auth = require("../auth");



//route for adding product by admin only
router.post("/",auth.verify,(req,res)=>{
	const data ={
		product: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	productController.addProduct(data).then(resultFromController=>res.send(resultFromController));
});
//route for getting all products
router.get("/all",(req,res)=>{
	productController.getAllProducts().then(resultFromController=>res.send(resultFromController));
});

router.get("/active",(req,res)=>{
	productController.getAllActiveProducts().then(resultFromController=>res.send(resultFromController));
});


//route for getting a specific product
router.get("/:productId",(req,res)=>{
	productController.getProduct(req.params).then(resultFromController=>res.send(resultFromController));
});

//Route for updating a product
// router.put("/:productId",auth.verify,(req,res)=>{
// 	productController.updateProduct(req.params,req.body).then(resultFromController=>res.send(resultFromController));
// });

router.put("/:productId",auth.verify,(req,res)=>{
	const data ={
	isAdmin: auth.decode(req.headers.authorization).isAdmin,
	product: req.body
	}
	productController.updateProduct(data,req.params).then(resultFromController=>res.send(resultFromController));
});

//Route for archiving a product

// router.put("/:productId/archive", auth.verify, (req, res) => {

// 	productController.archiveProduct(req.params).then(resultFromController => res.send(resultFromController));
	
// });

router.put("/:productId/archive", auth.verify, (req, res) => {
	const data ={
	isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	productController.archiveProduct(data,req.params).then(resultFromController => res.send(resultFromController));
	
});

// Route for activating a product
router.put("/:productId/activate", auth.verify, (req, res) => {
	const data ={
	isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	productController.activateProduct(data,req.params).then(resultFromController => res.send(resultFromController));
	
});
module.exports = router;