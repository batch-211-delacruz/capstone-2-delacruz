/*

App: E Commerce API

Scenario:
	An E Commerce application where a user can check the products to be offered

Type: E commerce  (Web App)

Description:

	An E commerce API where a user can check products and create order(checkout)
	Allows an admin to do CRUD operations
	Allows users to register into our database

Features:
	- User Registration
	- User authentication

	Customer/Authenticated Users;
	-View products 
	-Create order(products)

	Admin Users:
	-Add Products
	-Update Products
	-Archive/Unarchive a product (soft delete/reactivate the course)
	-View Products (All Products active/Inactive)
	-View/Manage User Accounts**

	All Users(guests)
	-View Active Products

*/

/*
Type: E Commerce API

3-Model Structure

User
firstName - string,
lastName - string,
email - string,
password - string,
mobileNo - string,
isAdmin - boolean,
	      default: false


Product
name - string,
description - string,
price - number
isActive - boolean
		   default: true,
createdOn - date
			default: new Date()
orders - [
	{
		orderId - string,
		quantity - number

	}
]


Order
userId - string
totalAmount - number,
purchasedOn - date
		     default: new Date(),
products - [

	{
		productId - string,
		quantity - number
	}

]


2-Model Structure

User
firstName - string,
lastName - string,
email - string,
password - string,
mobileNo - string,
isAdmin - boolean,
	      default: false
orders: [
	
	{
		totalAmount - number,
		purchasedOn - date
				     default: new Date(),
		products - [

			{
				productId - string,
				quantity - number
			}

		]
	}

]

Product
name - string,
description - string,
price - number
isActive - boolean
		   default: true,
createdOn - date
			default: new Date()
orders: [
	
	{	
		orderId - string,
		userId - string,
		quantity - number,
		purchasedOn - date
				     default: new Date(),
	}

]
*/