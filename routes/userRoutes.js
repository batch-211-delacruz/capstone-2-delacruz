const express = require("express");
const router = express.Router();
const userController = require("../controllers/userControllers");
const auth = require("../auth");

//check email
router.post("/checkEmail",(req,res)=>{
	userController.checkEmailExists(req.body).then(resultFromController=>res.send(resultFromController));
})


// Route for user registration
router.post("/register",(req,res)=>{
	userController.registerUser(req.body).then(resultFromController=>res.send(resultFromController));
})


//Route to get all users
router.get("/",(req,res)=>{
	userController.getAllUsers().then(resultFromController=>res.send(resultFromController));
})

//Route for user authentication
router.post("/login",(req,res)=>{
	userController.loginUser(req.body).then(resultFromController=>res.send(resultFromController));
});
// Route to make/update the user to admin
router.put("/:userId/toadmin",auth.verify,(req,res)=>{
	let data ={
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	userController.updateAdmin(data,req.params).then(resultFromController=>res.send(resultFromController));
});

//Route for order/checkout
router.post("/checkout/:productId",auth.verify,(req,res)=>{
	const data ={
		order: req.body,
		productId : req.body.products,
		products: req.body.products,
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
		userId: auth.decode(req.headers.authorization).id
	}
	userController.createOrder(data,req.params).then(resultFromController=>res.send(resultFromController));
});


//Route to get a users details

router.get("/getUserDetails",auth.verify,(req,res)=>{


	const userData = auth.decode(req.headers.authorization);

	userController.getUserDetails({userId:userData.id}).then(resultFromController=>res.send(resultFromController));
});
//
router.get("/allorder",auth.verify,(req,res)=>{
	let data ={
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	userController.getAllOrders(data).then(resultFromController=>res.send(resultFromController));

});

module.exports = router;