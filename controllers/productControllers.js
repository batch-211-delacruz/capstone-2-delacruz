const Product = require("../models/Product");



//adding products admin only

module.exports.addProduct = (data)=>{
    console.log(data);
    if(data.isAdmin){
        let newProduct = new Product({
                name : data.product.name,
                description : data.product.description,
                price : data.product.price
        });
        console.log(newProduct);
        return newProduct.save().then((product,error)=>{
            if(error){
                return false;
            }else {
                return product;
            };
        });
    }else {
        return false;
    }
}
//Retrieving all products
module.exports.getAllProducts = ()=>{
    return Product.find({}).then(result=>{
        return result;
    })
}


//Retrieving all active products

module.exports.getAllActiveProducts = ()=>{
    return Product.find({isActive:true}).then(result=>{
        return result;
    })
}

//Retrieving single product

module.exports.getProduct = (reqParams)=>{
    return Product.findById(reqParams.productId).then(result=>{
        return result;
    });
};

//Updating a product


//Admin only
module.exports.updateProduct = (data,reqParams)=>{
    console.log();
    if(data.isAdmin){
        let updatedProduct = {
            name : data.product.name,
            description: data.product.description,
            price : data.product.price
        };
        return Product.findByIdAndUpdate(reqParams.productId,updatedProduct).then((product,error)=>{
            if(error){
                return false;
            }else{
                return ({product,updatedProduct});
            }
        })
}else {
    return false;
};
}

// module.exports.updateProduct = (reqParams,reqBody) =>{

//     let updatedProduct ={
//         name: reqBody.name,
//         description: reqBody.description,
//         price:reqBody.price
//     };

//     return Product.findByIdAndUpdate(reqParams.productId,updatedProduct).then((product,error)=>{
       
//         if(error){
//             return false;
     
//         }else{
//             return true;
//         };
//     });
// };


// Archive a product

//archiving a product (admin only)
module.exports.archiveProduct = (data,reqParams) => {

    console.log(data);
    if(data.isAdmin){
    let updateActiveField = {
        isActive : false
    };
    return Product.findByIdAndUpdate(reqParams.productId, updateActiveField).then((product, error) => {
        if (error) {

            return false;

        } else {

            return true;

        };

    });
}else {
    return false;
};
}
//activating a product (admin only)
module.exports.activateProduct = (data,reqParams) => {

    console.log(data);
    if(data.isAdmin){
    let updateActiveField = {
        isActive : true
    };
    return Product.findByIdAndUpdate(reqParams.productId, updateActiveField).then((product, error) => {
        if (error) {

            return false;

        } else {

            return true;

        };

    });
}else {
    return false;
};
}
//


