const User = require("../models/User");
const Product = require("../models/Product");
const bcrypt = require("bcrypt");
const auth = require("../auth");


//Check email

module.exports.checkEmailExists = (reqBody) =>{
	// The result is sent back to the frontend via the "then" method found in the route file
	return User.find({email:reqBody.email}).then(result=>{
		// The "find" method returns a record if a match is found
		if(result.length>0){
			return true;
		// No duplicate email found
		// The user is not yet registered in the databas
		}else{
			return false;
		};
	});
};

// Registering a User
// module.exports.registerUser = (reqBody) =>{
// let newUser = new User({
// 		firstName : reqBody.firstName,
// 		lastName : reqBody.lastName,
// 		email : reqBody.email,
// 		mobileNo : reqBody.mobileNo,
// 		password : bcrypt.hashSync(reqBody.password,10)
// 	})

// 	return newUser.save().then((user,error)=>{
// 		if(error){
// 			return false;
// 		}else {
// 			return user;
// 			true
// 		};
// 	});
// };
module.exports.registerUser = (reqBody) =>{

	// Creates a variable "newUser" and instantiates a new "User" object using the mongoose model
	// Uses the information from the request body to provide all the necessary information
	let newUser = new User({
		firstName : reqBody.firstName,
		lastName : reqBody.lastName,
		email : reqBody.email,
		mobileNo : reqBody.mobileNo,
		// 10 is the value provided as the number of "salt" rounds that the bcrypt algorithm will run in order to encrypt the password
		password : bcrypt.hashSync(reqBody.password,10)
	})
	// Saves the created object to our database
	return newUser.save().then((user,error)=>{
		// User registration failed
		if(error){
			return false;
		// User registration successful
		}else{
			return true;
		};
	});
};

//Get  all registered Users
module.exports.getAllUsers =()=>{
	return User.find({}).then(result=>{
		return result;
	})
}


//log in a registered User
module.exports.loginUser = (reqBody) =>{
	return User.findOne({email:reqBody.email}).then(result=>{
		if(result==null){
			return false;
		}else{
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password,result.password);
			if(isPasswordCorrect){
				return { access: auth.createAccessToken(result)}
			}else{
				return false
			};
		};
	});
};
//Updating a User's role (admin only)
module.exports.updateAdmin = (data,reqParams)=>{

	if(data.isAdmin){
		let updateToAdmin = {
		isAdmin : true
	};
        return User.findByIdAndUpdate(reqParams.userId,updateToAdmin).then((user,error)=>{
            if(error){
                return false;
            }else{
                return "Successfully updated";
                true;
            }
        })
}else{
	return false;
}
}
//Retrieve User details


module.exports.getUserDetails =(data)=>{
	return User.findById(data.userId).then(result=>{
		result.password = "";
		return result;
	})
}

// Create order 
module.exports.createOrder = async (data,reqParams) =>{
	console.log(data);
	if(!data.isAdmin){
	let isUserUpdated = await User.findById(data.userId).then(user=>{

			user.orders.push({
				totalAmount : data.order.totalAmount,
				products : data.products
			})

		// let orders = user.orders 
		// console.log(`this is the order ${orders}`)

		return user.save().then((user,error)=>{
			if(error){
				return false;
			}else{
				return true;
			};
		});

	});
		let isProductUpdated = await Product.findById(reqParams.productId).then(product=>{
			console.log(`product${product}`)
					// let newProduct = new Product({
					// 	orders : data.order.quantity
					// });
					product.orders.push({
						orders : data.products
					})
					//product.orders.push({orders : data.order.quantity})
		//let products = product.orders
		//console.log(`this is the product${products}`)
		
		return product.save({orders : data.order.quantity}).then((product,error)=>{
			if(error){
				return false;
			}else{
				return true;
			};
		});
	});

	if(isUserUpdated&&isProductUpdated){
		return true;
	}else{
		return false;
	};
}else {
	return false
}
}

